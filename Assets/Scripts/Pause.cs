﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Pause : MonoBehaviour
{
    #region Singleton Class: Pause
    public static Pause Instance;
    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        }
    }
    #endregion
    [SerializeField] Button pauseButton;
    [SerializeField] Button homeButton;

    //private bool muted = false;
    private bool tapPause = false;
    //// Start is called before the first frame update
    void Start()
    {
        homeButton.onClick.AddListener(OnHomeButtonClicked);
        pauseButton.onClick.AddListener(OnPauseButtonClicked);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void OnHomeButtonClicked()
    {
        SceneManager.LoadScene("Home");
    }
    void OnPauseButtonClicked()
    {
        if (!tapPause)
        {
            tapPause = true;
            Time.timeScale = 0;
        }
        else
        {
            tapPause = false;
            Time.timeScale = 1;
        }
    }

}
