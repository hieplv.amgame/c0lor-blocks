﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Home : MonoBehaviour
{
    [SerializeField] Button playButton;
    [SerializeField] Button helpButton;
    [SerializeField] Button closeButton;
    [SerializeField] Button quitButton;
    [SerializeField] GameObject helpPanel;


    // Start is called before the first frame update
    void Start()
    {
        playButton.onClick.AddListener(OnPlayButtonClicked);
        helpButton.onClick.AddListener(OnHelpButtonClicked);
        closeButton.onClick.AddListener(OnCloseButtonClicked);
        quitButton.onClick.AddListener(OnQuitButtonClicked);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void OnPlayButtonClicked()
    {
        SceneManager.LoadScene("SelectLevel");
    }
    void OnHelpButtonClicked()
    {
        helpPanel.SetActive(true);
    }
    void OnCloseButtonClicked()
    {
        helpPanel.SetActive(false);
    }
    void OnQuitButtonClicked()
    {
        Application.Quit();
    }
}
