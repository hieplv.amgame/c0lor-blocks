﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HolePhysic : MonoBehaviour
{
    [SerializeField] OnChangePositon onChangePositon = default;
    
    // Start is called before the first frame update
    void Start()
    {
        GameObject[] AllGos = FindObjectsOfType(typeof(GameObject)) as GameObject[];
        foreach (var go in AllGos)
        {
            if (go.layer == LayerMask.NameToLayer("Obstacles") || go.layer == LayerMask.NameToLayer("Rewards"))
            {
                Physics.IgnoreCollision(go.GetComponent<Collider>(),onChangePositon.GeneratedMeshCollider, true);
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter(Collider other)
    {
        Physics.IgnoreCollision(other, onChangePositon.GroundCollider, true);
        Physics.IgnoreCollision(other, onChangePositon.GeneratedMeshCollider, false);
    }
    private void OnTriggerExit(Collider other)
    {
        Physics.IgnoreCollision(other, onChangePositon.GroundCollider, false);
        Physics.IgnoreCollision(other, onChangePositon.GeneratedMeshCollider, true);
    }
}
