﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;


public class Timer : MonoBehaviour
{
    public float timeValue = 90;
    [SerializeField] Image timeProgress;
    private float maxTime;
    // Start is called before the first frame update
    void Start()
    {
        maxTime = timeValue;
    }

    // Update is called once per frame
    void Update()
    {
        if (timeValue > 0)
        {
            timeValue -= Time.deltaTime;
            timeProgress.fillAmount = timeValue / maxTime;
        }
        else
        {
            timeValue = 0;
            Game.isGameover = true;
            Camera.main.transform.DOShakePosition(.4f, .2f, 20, 90f)
                .OnComplete(() =>
                {
                    Level.Instance.ResetCurrentLevel();
                });
        }
    }

}
