﻿
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using DG.Tweening;

public class _UIManager : MonoBehaviour
{
    #region Singleton class: UIManager

    public static _UIManager Instance;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }
    #endregion

    [Header("Level Progress UI")]

    [SerializeField] public int sceneOffset;
    [SerializeField] public TMP_Text nextLevelText;
    [SerializeField] public TMP_Text currentLevelText;
    [SerializeField] public Image progressFillImage;
   
    [Space]
    [SerializeField] public TMP_Text completeText;

    [Space]
    [SerializeField] Image fadePanel;

    // Start is called before the first frame update
    void Start()
    {
        FadePanelOnStart();
        progressFillImage.fillAmount = 0;
        SetLevelProgressText();
    }

    // Update is called once per frame
    void Update()
    {

    }
    public void SetLevelProgressText()
    {
        int level = SceneManager.GetActiveScene().buildIndex ;
        currentLevelText.text = level.ToString();
        nextLevelText.text = (level + 1).ToString();
    }

    public void UpdateLevelProgress()
    {
        float val = 1f - ((float)Level.Instance.objectsInScene / Level.Instance.totalObjects);
        //progressFillImage.fillAmount = val;
        progressFillImage.DOFillAmount(val, .4f);
    }
    public void ShowLevelCompleteText()
    {
        completeText.
            DOFade(255f, 1f).From(0f);
    }
    void FadePanelOnStart()
    {
        fadePanel.DOFade(0f,1.5f).From(255f);
    }
}
